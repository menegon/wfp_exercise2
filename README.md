# WFP Exercise 2 - Web GIS application prototype #

Through the Web GIS appllication the user can visualize an query the daily Active Fire Data downloaded from the Earth Observing System Data and Information System by NASA.

The Web GIS application is built with OpenLayers 3, Bootstrap 3 and JQuery. The server side is built with GeoDjango and Django-GeoJSON (https://pypi.python.org/pypi/django-geojson).

#### Layers ####
* Open Street Map (from the OSM Tile Server)
* Country: GeoJSON layer from the local Django / PostGIS
* Hotspot: GeoJSON layer from the local Django / PostGIS

The Hotspot layer is visualized using a cluster strategy.
Only the last daily hotspots are visualized according to the "lastimport" attribute (detailed explanation in https://bitbucket.org/menegon/wfp_exercise1).


## Installation
Clone the repository in a local directory

    git clone https://menegon@bitbucket.org/menegon/wfp_exercise2.git

Install the Requirements:

    pip install django
    pip install django-geojson


Edit the local_settings.py in order to customize the database connection settings.

Start the lightweight development Web server

    cd /path_of_exercise_repository/
    ./manage.py runserver


***

#### Home view ####
![figure](img/home.png)

***

#### Hotspot detail view ####
![figure](img/detail.png)