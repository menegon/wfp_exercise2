"""exercise2 URL Configuration
"""

from django.conf.urls import include, url
from django.contrib import admin
from exercise2.data.views import HomeView, HotspotDetailView, \
    CountryGeoJSONView, HotspotGeoJSONView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^data/hotspot/(?P<pk>[0-9]+)/$', HotspotDetailView.as_view(), name='hotspot_detail'),
    url(r'^data/country/geojson$', CountryGeoJSONView.as_view(), name='country_geojson'),
    url(r'^data/hotspot/geojson$', HotspotGeoJSONView.as_view(), name='hotspot_geojson'),
    url(r'^admin/', include(admin.site.urls)),
]
