from __future__ import absolute_import

from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.detail import DetailView
from djgeojson.views import GeoJSONLayerView
from . import models

class HomeView(TemplateView):
    template_name = "home.html"


class HotspotDetailView(DetailView):
    model = models.Hotspot


class CountryGeoJSONView(GeoJSONLayerView):
    model = models.Country
    # simplify = 0.1


class HotspotGeoJSONView(GeoJSONLayerView):
    model = models.Hotspot
    properties = ['scan', 'track', 'acq_date', 'acq_time', 'satellite']
