from __future__ import unicode_literals
from django.contrib.gis.db import models


class Country(models.Model):
    ogc_fid = models.AutoField(primary_key=True)
    geom = models.MultiPolygonField(srid=4326, db_column="wkb_geometry")
    iso2 = models.CharField(max_length=2, blank=True, null=True)
    iso3 = models.CharField(max_length=3, blank=True, null=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    pop2005 = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)

    objects = models.GeoManager()

    class Meta:
        db_table = 'country'


class HotspotManager(models.GeoManager):
    def get_queryset(self):
        return super(HotspotManager, self).get_queryset().filter(lastimport=True)


class Hotspot(models.Model):
    ogc_fid = models.AutoField(primary_key=True)
    geom = models.PointField(srid=4326, db_column="wkb_geometry")
    scan = models.DecimalField(max_digits=32, decimal_places=10, blank=True, null=True)
    track = models.DecimalField(max_digits=32, decimal_places=10, blank=True, null=True)
    acq_date = models.DateField(blank=True, null=True)
    acq_time = models.CharField(max_length=5, blank=True, null=True)
    satellite = models.CharField(max_length=1, blank=True, null=True)
    lastimport = models.BooleanField()

    objects = HotspotManager()

    class Meta:
        db_table = 'hotspot'
